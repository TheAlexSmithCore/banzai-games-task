namespace Core.Extendable
{
    public class IntField : ExtendableField<int>, INormalizable, IResettable
    {
        public IntField(int minValue, int maxValue) : base(minValue, maxValue)
        {
        }

        protected override void CalculateBorders()
        {
            if (CurrentValue < MinValue)
            {
                CurrentValue = MinValue;
            }

            if (CurrentValue > MaxValue)
            {
                CurrentValue = MaxValue;
            }
        }

        protected override int CalculateValueChange(int temp, int value, FieldType type = default, SetType setType = default)
        {
            switch (setType)
            {
                case SetType.Equate:
                    temp = value;
                    break;
                case SetType.Sum:
                    temp += value;
                    break;
                case SetType.Subtract:
                    temp -= value;
                    break;
                case SetType.Multiply:
                    temp *= value;
                    break;
                case SetType.Divide:
                    temp /= value;
                    break;
                
                default:
                    temp = value;
                    break;
            }

            return temp;
        }

        public float Normalized()
        {
            return (float)CurrentValue / MaxValue;
        }

        public void Reset()
        {
            CurrentValue = MaxValue;
        }
    }
}
