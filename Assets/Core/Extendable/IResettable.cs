namespace Core.Extendable
{
    public interface IResettable
    {
        public void Reset();
    }
}