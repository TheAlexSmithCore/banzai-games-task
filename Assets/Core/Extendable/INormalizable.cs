﻿namespace Core.Extendable
{
    public interface INormalizable
    {
        public float Normalized();
    }
}