namespace Core.UniversalComponents
{
    public class ComponentUpdater : UniversalComponent, IUpdatable
    {
        public ComponentUpdater(params IUpdatable[] components)
        {
            _components = components;
        }

        private readonly IUpdatable[] _components;

        public void Update()
        {
            if(!IsActive) {return; }

            foreach (IUpdatable t in _components)
            {
                t.Update();
            }
        }
    }
}
