﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.UniversalComponents
{
    public class UniversalComponentMono : MonoBehaviour, IComponent
    {
        private UniversalComponentMono _targetComponent;
        protected UniversalComponentMono TargetComponent
        {
            get => this;
            set => _targetComponent = value;
        }

        private bool _isActivated = true;
        public bool IsActive
        {
            get => _isActivated;
            set
            {
                _isActivated = value;

                if (_isActivated)
                {
                    OnActivated();
                }
                else
                {
                    OnDeactivated();
                }
            }
        }
        
        public virtual void OnActivated()
        {
            foreach (IComponent component in _components)
            {
                component.IsActive = true;
            }
        }

        public virtual void OnDeactivated()
        {
            foreach (IComponent component in _components)
            {
                component.IsActive = false;
            }
        }
                
        private readonly List<IComponent> _components = new List<IComponent>();
        
        private void AddComponent(IComponent component)
        {
            if (_components.Contains(component))
                throw new Exception("Component already exists. Component: " + component.GetType().Name);

            _components.Add(component);
        }
        
        private void RemoveComponent(IComponent component)
        {
            if (_components.Contains(component))
            {
                _components.Remove(component);
            }
        }

        public static UniversalComponentMono operator +(UniversalComponentMono universalComponent, IComponent component)
        {
            universalComponent.AddComponent(component);
            return universalComponent;
        }
        
        public static UniversalComponentMono operator -(UniversalComponentMono universalComponent, IComponent component)
        {
            universalComponent.RemoveComponent(component);
            return universalComponent;
        }
    }
}