namespace Core.UniversalComponents{
    public interface IUpdatable
    {
        public void Update();
    }
}
