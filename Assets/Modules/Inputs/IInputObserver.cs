namespace Modules.Inputs
{
    public interface IInputObserver
    {
        public void ObserveInput();
    }
}
