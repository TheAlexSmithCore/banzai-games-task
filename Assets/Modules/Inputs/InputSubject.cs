using Core.UniversalComponents;

namespace Modules.Inputs
{
    public class InputSubject : UniversalComponent, IUpdatable
    {
        public InputSubject(params IInputObserver[] observers)
        {
            _observers = observers;
        }
        
        private readonly IInputObserver[] _observers;
        
        public void Update()
        {
            foreach (IInputObserver observer in _observers)
            {
                observer?.ObserveInput();
            }
        }
    }
}
