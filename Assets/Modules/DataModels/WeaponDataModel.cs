using System.Collections.Generic;
using Modules.BattleSystem;
using Modules.BattleSystem.Data;

namespace Modules.DataModels
{
    public class WeaponDataModel
    {
        private readonly Dictionary<int, WeaponData> _data = new Dictionary<int, WeaponData>()
        {
            [0] = new WeaponData("Tank Rifle", WeaponType.Range, 10, .5f, 18f, "Tank/TankRifle"),
            [1] = new WeaponData("Tank Machine Gun", WeaponType.Range,2, .1f, 12f, "Tank/TankMachineGun"),
            [2] = new WeaponData("Tank Shotgun", WeaponType.Range,4, .6f, 16f, "Tank/TankShotgun"),
        };

        public WeaponData Get(int id)
        {
            if (!_data.ContainsKey(id))
                return default;
            
            return _data[id];
        }
    }
}
