using System.Collections.Generic;
using Modules.Entities.Data;
using UnityEngine;

namespace Modules.DataModels
{
    public class EntityDataModel
    {
        private readonly string[] _controllersPath = new string[2] {"Controllers/PlayerController", "Controllers/AIController"};
        private readonly int[] _possibleEnemies = new int[3] {1, 2, 3};
        
        private readonly Dictionary<int, EntityData> _data = new Dictionary<int, EntityData>()
        {
            [0] = new EntityData("Tank", 0, 100, .5f,5f,"Characters/SimpleTank"),
            [1] = new EntityData("Zombie", 0, 5,.75f,2f,"Characters/ZombieOriginal"),
            [2] = new EntityData("Zombie Runner", 0, 5, .9f, 4f, "Characters/ZombieRunner"),
            [3] = new EntityData("Zombie Giant", 0, 10, .35f, 1.5f, "Characters/ZombieGiant")
        };

        public EntityData Get(int id)
        {
            if (!_data.ContainsKey(id))
                return default;
            
            return _data[id];
        }

        public int GetRandomEnemyID()
        {
            return _possibleEnemies[Random.Range(0, _possibleEnemies.Length)];
        }

        public string GetControllerPath(int id)
        {
            return id > _controllersPath.Length ? null : _controllersPath[id];
        }
    }
}
