using Core.UniversalComponents;
using UnityEngine;

namespace Modules.Utilities
{
    public interface IPullElement
    {
        public void TranslateTo(Vector3 point);
        
        public void OnReset();
        public void OnReturnToPool();
    }
}
