using UnityEngine;

namespace Modules.Utilities
{
    public sealed class ObjectPuller
    {
        public ObjectPuller(Vector3 translatePoint, params IPullElement[] pullElements)
        {
            _translatePoint = translatePoint;
            _pullElements = pullElements;
        }

        private readonly Vector3 _translatePoint;
        private readonly IPullElement[] _pullElements;

        public void Pull()
        {
            foreach (IPullElement element in _pullElements)
            {
                element.OnReset();
                Translate(element, _translatePoint);
                element.OnReturnToPool();
            }
        }

        private void Translate(IPullElement element, Vector3 point)
        {
            element.TranslateTo(point);
        }

        public void TranslateAll(Vector3 point)
        {
            foreach (IPullElement element in _pullElements)
            {
                element.TranslateTo(point);
            }
        }
    }
}
