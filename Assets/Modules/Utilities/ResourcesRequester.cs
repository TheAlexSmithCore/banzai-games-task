using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Modules.Utilities
{
    public class ResourcesRequester<TResource> where TResource : Component
    {
        private TResource _loadedResource;
        
        public async Task RequestAsync(string path)
        {
            ResourceRequest request = Resources.LoadAsync<TResource>(path);

            while (!request.isDone)
            {
                await Task.Yield();
            }

            if(request == null)
                throw new Exception("Exception while loading resource at path: " + path);

            _loadedResource = (TResource) request.asset;
        }

        public void Request(string path)
        {
            _loadedResource = Resources.Load<TResource>(path);
        }

        public TResource Get()
        {
            return _loadedResource;
        }
    }
}
