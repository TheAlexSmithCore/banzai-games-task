using System;
using Modules.Utilities;

namespace Modules.Entities
{
    public interface ISpawnable : IPullElement
    {
        public void Spawn();
        public void Dead();

        public Action<ISpawnable> OnDead { get; set; }
    }
}
