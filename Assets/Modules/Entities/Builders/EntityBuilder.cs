using Modules.DataModels;
using Modules.Entities.Data;
using Modules.UniversalMatch;

namespace Modules.Entities.Builders
{
    public class EntityBuilder
    {
        public EntityBuilder(Entity entity)
        {
            _entity = entity;
        }

        private readonly Entity _entity;
        
        public void Build(ParticipantTeam team, EntityData data, BodyModificator modificator, WeaponDataModel weaponDataModel, ParticipantAggregator participantAggregator)
        {
            participantAggregator.Add(team, _entity);
            
            EntityInitializationContainer container = new EntityInitializationContainer(data, modificator, weaponDataModel,participantAggregator);

            _entity.Team = team;
            
            _entity.Init(container);
        }
    }
}
