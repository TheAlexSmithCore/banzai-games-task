using Core.UniversalComponents;
using Modules.BattleSystem;
using Modules.DataModels;
using Modules.Inputs;
using Modules.Movables;
using Modules.UniversalMatch.UI;
using UnityEngine;

namespace Modules.Entities
{
    public class PlayerEntity : Entity
    {
        [SerializeField] private Rigidbody rb;
        [HideInInspector] public WeaponHolder weaponHolder;

        private Shooter _shooter;
        private TankMotor _motor;
        private InputSubject _input;

        private WeaponDataModel _weaponDataModel;
        public ParticipantInfoView healthView;
        
        public override void Init(EntityInitializationContainer container)
        {
            _weaponDataModel = container.WeaponDataModel;

            OnHealthChanged += healthView.Refresh;
            
            weaponHolder = container.Modificator.weaponHolder;
            weaponHolder.Init(_weaponDataModel);
            
            base.Init(container);
        }

        protected override void InitComponents()
        {
            TargetComponent += _motor = new TankMotor(
                rb,
                Characteristics
            );

            TargetComponent += _shooter = new Shooter(
                _weaponDataModel,
                weaponHolder,
                Team,
                ParticipantAggregator
            );

            TargetComponent += _input = new InputSubject(
                _motor,
                _shooter,
                _shooter.WeaponSwitcher
            );
            
            TargetComponent += Updater = new ComponentUpdater(
                _input,
                _motor,
                _shooter
                );
        }
    }
}
