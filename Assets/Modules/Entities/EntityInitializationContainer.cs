using Modules.DataModels;
using Modules.Entities.Data;
using Modules.UniversalMatch;

namespace Modules.Entities
{
    public class EntityInitializationContainer
    {
        public EntityInitializationContainer(EntityData data, BodyModificator modificator, WeaponDataModel weaponDataModel, ParticipantAggregator participantAggregator)
        {
            Data = data;
            Modificator = modificator;
            WeaponDataModel = weaponDataModel;
            ParticipantAggregator = participantAggregator;
        }

        public readonly EntityData Data;
        public readonly BodyModificator Modificator;
        public readonly WeaponDataModel WeaponDataModel;
        public readonly ParticipantAggregator ParticipantAggregator;
    }
}
