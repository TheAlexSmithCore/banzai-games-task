using System;
using Core.Extendable;
using Core.UniversalComponents;
using Modules.UniversalMatch;
using UnityEngine;

namespace Modules.Entities{
    public abstract class Entity : UniversalComponentMono, IDamageable, ISpawnable, IParticipant
    {
        public Transform Owner => transform;
        public Transform bodyTransform;

        public ParticipantTeam Team { get; set; }
        protected ParticipantAggregator ParticipantAggregator;

        protected BodyModificator Modificator;
        protected ComponentUpdater Updater;
        protected CharacteristicsContainer Characteristics;

        public Action<FloatField> OnHealthChanged { get; set; } = delegate {  };
        public void DealDamage(float damage)
        {
            float calculatedDamage = damage * Characteristics.Armor;
            
            Characteristics.Health.Set(calculatedDamage, FieldType.Current, SetType.Subtract);

            if (Characteristics.Health.Get() <= 0)
            {
                Dead();
            }
            
            OnHealthChanged.Invoke(Characteristics.Health);
        }

        public virtual void Init(EntityInitializationContainer container)
        {
            ParticipantAggregator = container.ParticipantAggregator;
            
            Characteristics = new CharacteristicsContainer(
                container.Data.MinHealth, 
                container.Data.MaxHealth,
                container.Data.Armor,
                container.Data.MoveSpeed
                );

            Modificator = container.Modificator;

            InitComponents();

            OnHealthChanged.Invoke(Characteristics.Health);
        }

        protected abstract void InitComponents();

        private void Update()
        {
            Updater?.Update();
        }

        public void Spawn()
        {
        }

        public void Dead()
        {
            Characteristics.Reset();
            OnDead.Invoke(this);
        }

        public Action<ISpawnable> OnDead { get; set; } = delegate {  };
        public void TranslateTo(Vector3 point)
        {
            transform.position = point;
        }

        public void OnReset()
        {
            IsActive = false;
        }

        public void OnReturnToPool()
        {
            IsActive = true;
        }
    }
}
