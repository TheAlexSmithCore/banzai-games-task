using Core.UniversalComponents;
using UnityEngine;

namespace Modules.Entities.AI
{
    public class SuicideRushState : AIBrainState, IUpdatable
    {

        public SuicideRushState()
        {
            
        }
        
        public override void OnEnter()
        {
            
        }

        public override void OnExit()
        {
            
        }

        public override void Call()
        {
            
        }

        public void Update()
        {
        }
    }
}
