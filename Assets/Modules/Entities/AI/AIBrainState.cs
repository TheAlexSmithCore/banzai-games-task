namespace Modules.Entities.AI
{
    public abstract class AIBrainState
    {
        protected AIBrainState()
        {
        }
        
        public abstract void OnEnter();
        public abstract void OnExit();
        
        public abstract void Call();
    }
}
