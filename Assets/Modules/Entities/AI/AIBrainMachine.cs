using System;
using Core.UniversalComponents;
using Modules.Movables;
using Modules.UniversalMatch;
using UnityEngine;
using UnityEngine.AI;

namespace Modules.Entities.AI
{
    public class AIBrainMachine : UniversalComponent, IUpdatable
    {
        public AIBrainMachine(NavMeshAgent agent, CharacteristicsContainer characteristics, BodyModificator modificator, ParticipantAggregator participantAggregator)
        {
            _agent = agent;
            _participantAggregator = participantAggregator;
            
            _conditionsContainer = new ConditionsContainer();
            
            _motor = new AIMotor(
                    agent,
                    characteristics,
                    modificator,
                    _conditionsContainer
                );

            _characteristics = characteristics;
            
            ChooseClosestTarget();
        }
        
        private float _refreshTimer;
        private const float TargetRefreshTime = 5f;
        private const float DistanceToKamikadze = 1f;

        private readonly AIMotor _motor;
        
        private readonly NavMeshAgent _agent;
        private readonly ParticipantAggregator _participantAggregator;
        private readonly ConditionsContainer _conditionsContainer;
        private readonly CharacteristicsContainer _characteristics;
        
        public void Update()
        {
            _motor.Update();
            
            if(_conditionsContainer.Target != null)
                TryKamikadze();
            
            if (Time.time > _refreshTimer)
            {
                ChooseClosestTarget();
            }
        }

        
        // Если честно, тут сделал халтурно
        public Action OnKamikadze = delegate { };
        private void TryKamikadze()
        {
            float distance = (_conditionsContainer.Target.Owner.position - _agent.transform.position).magnitude;

            if (distance < DistanceToKamikadze)
            {
                _conditionsContainer.Target.DealDamage(_characteristics.Health.Get());
                OnKamikadze.Invoke();
            }
        }

        private void ChooseClosestTarget()
        {
            _refreshTimer = Time.deltaTime + TargetRefreshTime;
            IDamageable[] enemies = _participantAggregator.GetEnemies(ParticipantTeam.Blue);

            _conditionsContainer.Target = CalculateClosest(enemies);
        }
        
        private IDamageable CalculateClosest(IDamageable[] targets)
        {
            int i = 0;
            int closestID = 0;
            float closestDistance = float.MaxValue;

            foreach (IDamageable target in targets)
            {
                float distance = (target.Owner.position - _agent.transform.position).magnitude;

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestID = i;
                }

                i++;
            }

            return targets[closestID];
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            _motor.IsActive = false;
        }

        public override void OnActivated()
        {
            base.OnActivated();
            _motor.IsActive = true;
        }
    }
}
