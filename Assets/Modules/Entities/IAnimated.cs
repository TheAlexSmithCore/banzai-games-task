namespace Modules.Entities
{
    public interface IAnimated
    {
        public void SetAnimatorValues();
    }
}
