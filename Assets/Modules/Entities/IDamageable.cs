using System;
using Core.Extendable;
using UnityEngine;

namespace Modules.Entities
{
    public interface IDamageable
    {
        public Transform Owner { get; }
        
        public Action<FloatField> OnHealthChanged { get; set; } 
        
        public void DealDamage(float damage);
    }
}
