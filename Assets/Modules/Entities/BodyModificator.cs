using Core.UniversalComponents;
using Modules.BattleSystem;
using UnityEngine;

namespace Modules.Entities
{
    public class BodyModificator : UniversalComponentMono
    {
        public Animator animator;
        public WeaponHolder weaponHolder;
    }
}
