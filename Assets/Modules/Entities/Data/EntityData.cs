namespace Modules.Entities.Data
{
    public struct EntityData
    {
        public EntityData(string displayName, int minHealth, int maxHealth, float armor, float moveSpeed, string bodyResourcePath)
        {
            DisplayName = displayName;

            MinHealth = minHealth;
            MaxHealth = maxHealth;
            Armor = armor;
            
            MoveSpeed = moveSpeed;

            BodyResourcePath = bodyResourcePath;
        }

        public readonly string DisplayName;

        public readonly int MinHealth;
        public readonly int MaxHealth;
        public readonly float Armor;

        public readonly float MoveSpeed;

        public readonly string BodyResourcePath;
    }
}
