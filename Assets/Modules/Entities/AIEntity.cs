using Core.UniversalComponents;
using Modules.Entities.AI;
using UnityEngine;
using UnityEngine.AI;

namespace Modules.Entities
{
    public class AIEntity : Entity
    {
        [SerializeField] private NavMeshAgent agent;

        private AIBrainMachine _brainMachine;
        
        protected override void InitComponents()
        {
            TargetComponent += _brainMachine = new AIBrainMachine(
                agent,
                Characteristics,
                Modificator,
                ParticipantAggregator
            );

            _brainMachine.OnKamikadze += Dead;

            Updater = new ComponentUpdater(
                _brainMachine
                );
        }
    }
}
