using Core.Extendable;

namespace Modules.Entities
{
    public class CharacteristicsContainer
    {
        public CharacteristicsContainer(int minHealth, int maxHealth, float armor, float moveSpeed)
        {
            _resettables = new IResettable[1]
            {
                Health = new FloatField(minHealth, maxHealth)
            };

            Armor = armor;

            MoveSpeed = moveSpeed;
        }

        private readonly IResettable[] _resettables;
        
        public readonly FloatField Health;
        public readonly float Armor;

        public readonly float MoveSpeed;

        public void Reset()
        {
            foreach (IResettable resettable in _resettables)
            {
                resettable.Reset();
            }
        }
    }
}
