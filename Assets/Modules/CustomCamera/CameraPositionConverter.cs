using System;
using System.Collections.Generic;
using Core.UniversalComponents;
using Modules.BattleSystem;
using UnityEngine;

namespace Modules.CustomCamera
{
    public class CameraPositionConverter : IUpdatable
    {
        public CameraPositionConverter(Camera camera)
        {
            _camera = camera;
        }

        private readonly Camera _camera;
        
        private readonly List<IMouseLookObserver> _screenToWorldListeners = new List<IMouseLookObserver>();

        public void Add(IMouseLookObserver listener)
        {
            if (_screenToWorldListeners.Contains(listener))
                throw new Exception("Observer contains in this list");
            
            _screenToWorldListeners.Add(listener);
        }

        public void Remove(IMouseLookObserver listener)
        {
            if (_screenToWorldListeners.Contains(listener))
            {
                _screenToWorldListeners.Remove(listener);
            }
        }

        public bool IsObjectInViewport(Vector3 point)
        {
            Vector3 viewportPoint = _camera.WorldToViewportPoint(point);

            if (viewportPoint.z < 0)
                return false;

            if (viewportPoint.x > 1 || viewportPoint.x < -1)
                return false;

            if (viewportPoint.y > 1 || viewportPoint.y < -1)
                return false;
            
            return true;
        }

        private Vector3 _mouseToWorld;

        public void Update()
        {
            Plane plane = new Plane(Vector3.up, 0);
            
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (plane.Raycast(ray, out float distance))
            {
                _mouseToWorld = ray.GetPoint(distance);
            }
            
            for (int i = 0; i < _screenToWorldListeners.Count; i++)
            {
                _screenToWorldListeners[i].Notify(_mouseToWorld);
            }
        }
    }
}
