using UnityEngine;

namespace Modules.CustomCamera
{
    public enum CameraType
    {
        Freeze = 0,
        TopDown = 1,
        Cinema = 2
    }
    
    [System.Serializable]
    public struct CameraParameters
    {
        public float height;
        public float distance;
        public float angle;

        [Space]
        public Vector3 offset;
    }
}
