using Core.UniversalComponents;
using UnityEngine;

namespace Modules.CustomCamera
{
    public interface ICustomCamera : IUpdatable
    {
        public Camera Camera { get; set; }
        public void ChangeTarget(Transform target);
    }
}
