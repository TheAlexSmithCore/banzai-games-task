using UnityEngine;

namespace Modules.CustomCamera
{
    public interface IScreenToWorldListener
    { 
        public void Notify(Vector3 position);
    }
}
