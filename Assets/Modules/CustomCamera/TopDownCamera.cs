using UnityEngine;

namespace Modules.CustomCamera
{
    public class TopDownCamera : ICustomCamera
    {
        public TopDownCamera(Camera camera, Transform target,CameraParameters parameters)
        {
            Camera = camera;
            _target = target;
            
            _parameters = parameters;
        }
        
        public Camera Camera { get; set; }

        private readonly CameraParameters _parameters;
        private Transform _target;

        public void Update()
        {
            Vector3 targetOffset = Vector3.zero;
            
            if (_target != null)
            {
                targetOffset = _target.position + _parameters.offset;
            }

            Vector3 flatPosition = targetOffset;
            flatPosition.y = 0;

            Vector3 finalPosition = flatPosition + CameraHandle();
            Camera.transform.position = finalPosition;

            Camera.transform.LookAt(targetOffset);
        }

        private Vector3 CameraHandle()
        {
            Vector3 worldPosition = (Vector3.forward * -_parameters.distance) + (Vector3.up * _parameters.height);
            Vector3 worldRotation = Quaternion.AngleAxis(_parameters.angle, Vector3.up) * worldPosition;

            return worldRotation;
        }

        public void ChangeTarget(Transform target)
        {
            _target = target;
        }
    }
}
