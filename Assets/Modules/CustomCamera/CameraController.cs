using Core.UniversalComponents;
using UnityEngine;

namespace Modules.CustomCamera
{
    [ExecuteInEditMode]
    public class CameraController : UniversalComponentMono
    {
        private Transform _target;

        private Camera _currentCamera;
        private ICustomCamera _customCamera;

        private ComponentUpdater _updater;
        public CameraPositionConverter CameraPositionConverter;

        [SerializeField] private CameraParameters parameters;

        private void Awake()
        {
            _currentCamera = Camera.main;

            ChangeCameraType(CameraType.TopDown);
            
            CameraPositionConverter = new CameraPositionConverter(_currentCamera);
            
            _updater = new ComponentUpdater(
                _customCamera,
                CameraPositionConverter
                );
        }

        public void ChangeTarget(Transform target)
        {
            _target = target;
            _customCamera?.ChangeTarget(_target);
        }

        private void ChangeCameraType(CameraType type)
        {
            switch (type)
            {
                case CameraType.Freeze:
                    _customCamera = null;
                    break;
                case CameraType.TopDown:
                    _customCamera = new TopDownCamera(_currentCamera, _target, parameters);
                    break;
                case CameraType.Cinema:
                    break;
            }
        }

        private void LateUpdate()
        {
            _updater?.Update();
        }
    }
}
