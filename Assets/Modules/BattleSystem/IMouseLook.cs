using UnityEngine;

namespace Modules.BattleSystem
{
    public interface IMouseLookObserver
    {
        public void Notify(Vector3 point);
    }
}
