using Modules.BattleSystem.Visualizers;
using UnityEngine;

namespace Modules.BattleSystem
{
    public class WeaponView : MonoBehaviour
    {
        public Transform[] shootingPoint;

        public AttackVisualizer visualizer;
    }
}
