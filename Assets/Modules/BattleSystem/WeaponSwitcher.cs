using System;
using Modules.Inputs;
using UnityEngine;

namespace Modules.BattleSystem
{
    public class WeaponSwitcher : IWeaponSwitcher, IInputObserver
    {
        public WeaponSwitcher(int maxWeaponCount)
        {
            _maxWeaponCount = maxWeaponCount;
        }
        
        private int _currentWeaponID;

        private int CurrentWeaponID
        {
            get => _currentWeaponID;
            set => _currentWeaponID = CalculateBorders(value);
        }
        
        private int _switchAxis;
        private readonly int _maxWeaponCount;

        public Action<int> OnWeaponSwitched = delegate(int id) {  };
        
        public void Switch()
        {
            CurrentWeaponID += _switchAxis;
            OnWeaponSwitched.Invoke(CurrentWeaponID);
        }

        private int CalculateBorders(int value)
        {
            if (value >= _maxWeaponCount)
            {
                return 0;
            }

            if (value < 0)
            {
                return _maxWeaponCount - 1;
            }

            return value;
        }

        public void ObserveInput()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                _switchAxis = -1;
                Switch();
            }
            
            if(Input.GetKeyDown(KeyCode.E))
            {
                _switchAxis = 1;
                Switch();
            }
        }
    }
}
