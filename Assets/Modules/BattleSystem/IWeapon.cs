using System;
using Modules.BattleSystem.Data;

namespace Modules.BattleSystem
{
    public interface IWeapon
    {
        public Action<ImpactData> OnShoot { get; set; }

        public void SwitchDetector(WeaponType type);
        public void Attack();
        public bool IsAttackAvailable();
    }
}
