using Core.UniversalComponents;
using Modules.BattleSystem.Data;
using Modules.BattleSystem.Detectors;
using Modules.DataModels;
using Modules.Inputs;
using Modules.UniversalMatch;
using UnityEngine;

namespace Modules.BattleSystem
{
    public class Shooter : UniversalComponent, IUpdatable, IInputObserver
    {
        public Shooter(WeaponDataModel weaponDataModel, WeaponHolder weaponHolder, ParticipantTeam alliesTeam, ParticipantAggregator participantAggregator)
        {
            _weaponDataModel = weaponDataModel;
            
            WeaponSwitcher = new WeaponSwitcher(3) {OnWeaponSwitched = WeaponSwitched};

            _weaponHolder = weaponHolder;
            _participantAggregator = participantAggregator;
            _alliesTeam = alliesTeam;

            SetWeapon(0);
        }

        private readonly ParticipantTeam _alliesTeam;
        private readonly WeaponDataModel _weaponDataModel;
        
        public readonly WeaponSwitcher WeaponSwitcher;
        private Weapon _weapon;

        private readonly WeaponHolder _weaponHolder;
        private readonly ParticipantAggregator _participantAggregator;
        
        private bool _isShooting;

        private void SetWeapon(int id)
        {
            WeaponData weaponData = _weaponDataModel.Get(id);

            _weaponHolder.Switch(id);
            
            _weapon = new Weapon(
                new DetectorDataContainer(
                        _weaponHolder.GetShootingPoint(),
                        weaponData,
                        _alliesTeam,
                        _participantAggregator
                    )
                );

            _weapon.SwitchDetector(weaponData.Type);

            _weapon.OnShoot += _weaponHolder.Visualize;
        }
        
        private void WeaponSwitched(int weaponID)
        {
            SetWeapon(weaponID);
        } 

        public void Update()
        {
            if (_isShooting && _weapon.IsAttackAvailable())
            {
                _weapon.Attack();
            }
        }

        public void ObserveInput()
        {
            _isShooting = Input.GetKey(KeyCode.X) || Input.GetMouseButton(0);
        }
    }
}
