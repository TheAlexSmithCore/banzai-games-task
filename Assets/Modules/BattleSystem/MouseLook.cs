using UnityEngine;

namespace Modules.BattleSystem
{
    public class MouseLook
    {
        public MouseLook(Vector3 axis)
        {
            _axis = axis;
        }

        private Vector3 _rotateTo;
        private readonly Vector3 _axis;

        private Vector3 _smoothVelocity = Vector3.zero;

        public void Rotate(Transform owner, Vector3 targetPoint)
        {
            _rotateTo = Vector3.SmoothDamp(_rotateTo, targetPoint, ref _smoothVelocity, Time.deltaTime * 5);
            _rotateTo.y = owner.position.y;
            
            owner.rotation = Quaternion.LookRotation(_rotateTo - owner.position, _axis);
        }
    }
}
