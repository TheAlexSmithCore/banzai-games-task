using System.Collections.Generic;
using Modules.Entities;
using UnityEngine;

namespace Modules.BattleSystem.Data
{
    public struct ImpactData
    {
        public ImpactData(Vector3 startPoint, Vector3 hitPoint, List<IDamageable> targets)
        {
            StartPoint = startPoint;
            HitPoint = hitPoint;

            Targets = targets;
        }
        
        public Vector3 StartPoint;
        public Vector3 HitPoint;

        public List<IDamageable> Targets;
    }
}
