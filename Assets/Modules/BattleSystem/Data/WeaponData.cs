namespace Modules.BattleSystem.Data
{
    public struct WeaponData
    {
        public WeaponData(string displayName, WeaponType type, int damage, float attackRate, float attackRadius, string resourcePath)
        {
            DisplayName = displayName;
            Type = type;
            
            Damage = damage;
            AttackRate = attackRate;

            AttackRadius = attackRadius;

            ResourcePath = resourcePath;
        }

        public readonly string DisplayName;

        public readonly WeaponType Type;
        
        public readonly int Damage;
        public readonly float AttackRate;

        public readonly float AttackRadius;

        public readonly string ResourcePath;
    }
}
