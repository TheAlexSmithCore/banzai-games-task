using System;
using Modules.BattleSystem.Data;
using Modules.BattleSystem.Detectors;
using UnityEngine;

namespace Modules.BattleSystem
{
    public class Weapon : IWeapon
    {
        public Weapon(DetectorDataContainer detectorDataContainer)
        {
            _detectorFactory = new AttackDetectorFactory(detectorDataContainer);

            Data = detectorDataContainer.WeaponData;
            SwitchDetector(Data.Type);
        }
        
        private WeaponData Data { get;}
        public Action<ImpactData> OnShoot { get; set; } = delegate {  };

        private readonly AttackDetectorFactory _detectorFactory;
        private IAttackDetector _detector;
        
        private float _lastShootTime;
        
        public void SwitchDetector(WeaponType type)
        {
            _detector = _detectorFactory.Get(type);
        }

        public void Attack()
        {
            ImpactData hitData = _detector.DetectEnemy();
            
            OnShoot.Invoke(hitData);

            for (int i = 0; i < hitData.Targets.Count; i++)
            {
                if(hitData.Targets[i] == null) {continue;}
                
                hitData.Targets[i].DealDamage(Data.Damage);
            }
        }

        public bool IsAttackAvailable()
        {
            if (Time.time < _lastShootTime)
                return false;

            _lastShootTime = Time.time + Data.AttackRate;
            return true;
        }
    }
}
