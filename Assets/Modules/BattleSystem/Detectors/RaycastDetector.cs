using System.Collections.Generic;
using Modules.BattleSystem.Data;
using Modules.Entities;
using Modules.UniversalMatch;
using UnityEngine;

namespace Modules.BattleSystem.Detectors
{
    public class RaycastDetector : IAttackDetector
    {
        public RaycastDetector(Transform[] origins, WeaponData data, ParticipantTeam alliesTeam, ParticipantAggregator participantAggregator)
        {
            _origins = origins;
            _data = data;
            _alliesTeam = alliesTeam;

            _participants = participantAggregator;
        }

        private readonly ParticipantAggregator _participants;

        private readonly WeaponData _data;
        private readonly ParticipantTeam _alliesTeam;
        
        private readonly Transform[] _origins;
        private Ray _ray;

        public ImpactData DetectEnemy()
        {
            ImpactData result = default;
            
            foreach (Transform origin in _origins)
            {
                _ray = new Ray(origin.position, origin.forward);
            
                result = new ImpactData(_ray.origin, _ray.origin + _ray.direction * _data.AttackRadius, new List<IDamageable>());
            
                if (!Physics.Raycast(_ray, out RaycastHit hit, _data.AttackRadius))
                    return result;

                result.HitPoint = hit.point;

                IDamageable[] targets = _participants.GetEnemies(_alliesTeam);

                foreach (IDamageable target in targets)
                {
                    if (target.Owner.gameObject != hit.collider.gameObject) continue;
                
                    result.Targets.Add(target);
                    break;
                }
            }

            return result;
        }
    }
}
