using Modules.BattleSystem.Data;
using Modules.UniversalMatch;
using UnityEngine;

namespace Modules.BattleSystem.Detectors
{
    public struct DetectorDataContainer
    {
        public DetectorDataContainer(Transform[] origins, WeaponData weaponData, ParticipantTeam alliesTeam, ParticipantAggregator participantAggregator)
        {
            Origins = origins;
            WeaponData = weaponData;
            AlliesTeam = alliesTeam;
            ParticipantAggregator = participantAggregator;
        }
        
        public readonly Transform[] Origins;
        public readonly WeaponData WeaponData;
        public readonly ParticipantTeam AlliesTeam;
        public readonly ParticipantAggregator ParticipantAggregator;
    }
}
