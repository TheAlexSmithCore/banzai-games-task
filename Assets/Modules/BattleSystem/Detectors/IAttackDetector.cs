﻿using Modules.BattleSystem.Data;

namespace Modules.BattleSystem.Detectors
{
    public interface IAttackDetector
    {
        public ImpactData DetectEnemy();
    }
}