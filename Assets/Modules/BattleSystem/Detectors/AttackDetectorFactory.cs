namespace Modules.BattleSystem.Detectors
{
    public class AttackDetectorFactory
    {
        public AttackDetectorFactory(DetectorDataContainer dataContainer)
        {
            _dataContainer = dataContainer;
        }

        private readonly DetectorDataContainer _dataContainer;

        public IAttackDetector Get(WeaponType type)
        {
            switch (type)
            {
                case WeaponType.Melee:
                    
                    break;
                case WeaponType.Range:
                    return new RaycastDetector(_dataContainer.Origins, _dataContainer.WeaponData, _dataContainer.AlliesTeam, _dataContainer.ParticipantAggregator);
                
                case WeaponType.Area:
                    return new AreaDetector(_dataContainer.Origins, _dataContainer.WeaponData, _dataContainer.AlliesTeam, _dataContainer.ParticipantAggregator);
                
                case WeaponType.Canon:
                    
                    break;
                
                default:
                    return null;
            }

            return null;
        }
    }
}
