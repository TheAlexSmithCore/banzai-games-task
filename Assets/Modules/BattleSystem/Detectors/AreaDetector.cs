using System.Collections.Generic;
using Modules.BattleSystem.Data;
using Modules.Entities;
using Modules.UniversalMatch;
using UnityEngine;

namespace Modules.BattleSystem.Detectors
{
    public class AreaDetector : IAttackDetector
    {
        public AreaDetector(Transform[] origins, WeaponData weaponData, ParticipantTeam alliesTeam, ParticipantAggregator participantAggregator)
        {
            _origins = origins;
            _weaponData = weaponData;

            _alliesTeam = alliesTeam;

            _participants = participantAggregator;
        }

        private readonly ParticipantTeam _alliesTeam;
        
        private readonly Transform[] _origins;
        private readonly WeaponData _weaponData;

        private readonly ParticipantAggregator _participants;

        public ImpactData DetectEnemy()
        {
            ImpactData result = default;
            
            foreach (Transform origin in _origins)
            {
                Collider[] colliders = {};
            
                result = new ImpactData(origin.position, origin.forward, new List<IDamageable>());

                var size = Physics.OverlapSphereNonAlloc(result.StartPoint, _weaponData.AttackRadius, colliders);

                if (size < 0)
                    return result;
            
                IDamageable[] targets = _participants.GetEnemies(_alliesTeam);

                foreach (Collider collider in colliders)
                {
                    foreach (IDamageable target in targets)
                    {
                        if (target.Owner.gameObject != collider.gameObject) continue;
                
                        result.Targets.Add(target);
                    }
                }
            }

            return result;
        }
    }
}
