using Modules.BattleSystem.Data;
using UnityEngine;

namespace Modules.BattleSystem.Visualizers
{
    public abstract class AttackVisualizer : MonoBehaviour, IAttackVisualizer
    {
        public abstract void Visualize(ImpactData data);
    }
}
