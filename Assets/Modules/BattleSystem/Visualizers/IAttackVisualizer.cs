using Modules.BattleSystem.Data;

namespace Modules.BattleSystem.Visualizers
{
    public interface IAttackVisualizer
    {
        public void Visualize(ImpactData data);
    }
}
