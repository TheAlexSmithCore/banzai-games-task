using System;
using Core.UniversalComponents;
using Modules.BattleSystem.Data;
using UnityEngine;

namespace Modules.BattleSystem.Visualizers
{
    public class TracerPool : UniversalComponentMono
    {
        [SerializeField] private TrailRenderer[] trails;
        private int _lastTrailIndex;
        
        public void Pull(ImpactData data)
        {
            TrailRenderer trail = trails[_lastTrailIndex];
            
            trail.gameObject.SetActive(true);
            
            trail.Clear();
            trail.AddPosition(data.StartPoint);
            trail.AddPosition(data.HitPoint);
                
            MoveNext();    
        }

        private void MoveNext()
        {
            _lastTrailIndex++;

            if (_lastTrailIndex >= trails.Length)
            {
                _lastTrailIndex = 0;
            }
        }
    }
}
