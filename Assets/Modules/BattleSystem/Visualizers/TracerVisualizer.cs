using Modules.BattleSystem.Data;
using UnityEngine;

namespace Modules.BattleSystem.Visualizers
{
    public class TracerVisualizer : AttackVisualizer
    {
        [SerializeField] private TrailRenderer trailRenderer;

        public override void Visualize(ImpactData data)
        {
            trailRenderer.Clear();
            
            trailRenderer.AddPosition(data.StartPoint);
            trailRenderer.AddPosition(data.HitPoint);
            //trailRenderer.enabled = false;
        }
    }
}
