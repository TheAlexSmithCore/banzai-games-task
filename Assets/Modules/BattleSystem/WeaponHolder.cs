﻿using System.Collections.Generic;
using Modules.BattleSystem.Data;
using Modules.DataModels;
using Modules.Utilities;
using UnityEngine;

namespace Modules.BattleSystem
{
    public class WeaponHolder : MonoBehaviour, IMouseLookObserver
    {
        private WeaponView _viewSelected;
        
        private readonly Dictionary<int, WeaponView> _views = new Dictionary<int, WeaponView>();

        private MouseLook _mouseLook;

        private WeaponDataModel _dataModel;
        
        public void Init(WeaponDataModel dataModel)
        {
            _dataModel = dataModel;
        }

        private void Start()
        {
            _mouseLook = new MouseLook(Vector3.up);
        }
        
        public void Visualize(ImpactData data)
        {
            _viewSelected.visualizer.Visualize(data);
        }

        public void Switch(int id)
        {
            if(_viewSelected != null)
                _viewSelected.gameObject.SetActive(false);

            WeaponView temp = GetView(id);

            temp.gameObject.SetActive(true);
            _viewSelected = temp;
        }

        public Transform[] GetShootingPoint()
        {
            return _viewSelected.shootingPoint;
        }

        private WeaponView GetView(int id)
        {
            if (_views.ContainsKey(id))
            {
                return _views[id];
            }

            string path = _dataModel.Get(id).ResourcePath;
            ResourcesRequester<WeaponView> requester = new ResourcesRequester<WeaponView>();
            requester.Request(path);

            WeaponView view = Instantiate(requester.Get(), transform, false);
            _views.Add(id, view);
            
            return view;
        }

        public void Notify(Vector3 point)
        {
            _mouseLook?.Rotate(_viewSelected.transform, point);
        }
    }
}