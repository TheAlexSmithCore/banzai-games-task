namespace Modules.BattleSystem
{
    public interface IWeaponSwitcher
    {
        public void Switch();
    }
}
