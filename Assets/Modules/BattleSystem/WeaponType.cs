namespace Modules.BattleSystem
{
    public enum WeaponType : byte
    {
        Melee = 0,
        Range = 1,
        Area = 2,
        Canon = 3
    }
}
