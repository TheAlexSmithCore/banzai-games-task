using System.Collections.Generic;
using Core.UniversalComponents;
using Modules.Entities;
using Modules.Entities.AI;
using Modules.UniversalMatch;
using UnityEngine;
using UnityEngine.AI;

namespace Modules.Movables
{
    public class AIMotor : UniversalComponent, IMovable, IAnimated
    {
        public AIMotor(NavMeshAgent agent, CharacteristicsContainer characteristics, BodyModificator modificator, ConditionsContainer conditionsContainer)
        {
            _agent = agent;
            _conditionsContainer = conditionsContainer;

            _modificator = modificator;

            ApplyCharacteristics(characteristics);
        }

        private readonly NavMeshAgent _agent;
        private readonly ConditionsContainer _conditionsContainer;

        private BodyModificator _modificator;

        public void Update()
        {
            SetAnimatorValues();
            CalculateMove();
            
            _agent.SetDestination(MoveVector);

        }

        public void SetAnimatorValues()
        {
            _modificator.animator.SetFloat("Move", _agent.velocity.normalized.magnitude);
        }

        public Vector3 MoveVector { get; private set; }
        public void CalculateMove()
        {
            if(_conditionsContainer.Target == null)
                return;

            MoveVector = _conditionsContainer.Target.Owner.position;
        }


        private void ApplyCharacteristics(CharacteristicsContainer characteristics)
        {
            _agent.speed = characteristics.MoveSpeed;
        }

        public override void OnActivated()
        {
            base.OnActivated();

            _agent.isStopped = false;
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();

            _agent.isStopped = true;
        }
    }
}
