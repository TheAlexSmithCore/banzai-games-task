using Core.UniversalComponents;
using Modules.Entities;
using Modules.Entities.Data;
using Modules.Inputs;
using UnityEngine;

namespace Modules.Movables
{
    public class TankMotor : UniversalComponent, IMovable, IInputObserver
    {
        public TankMotor(Rigidbody rigidbody, CharacteristicsContainer characteristics)
        {
            _rigidbody = rigidbody;
            _characteristics = characteristics;
        }

        private CharacteristicsContainer _characteristics;
        
        private Rigidbody _rigidbody;

        private float _verticalInput;
        private float _horizontalInput;
        
        public void Update()
        {
            CalculateMove();

            Transform owner = _rigidbody.transform;

            _rigidbody.velocity = MoveVector * _characteristics.MoveSpeed;
            owner.eulerAngles += Vector3.up * _horizontalInput;
        }

        public Vector3 MoveVector { get; private set; }
        public void CalculateMove()
        {
            MoveVector = _rigidbody.transform.forward * _verticalInput;
        }

        public void ObserveInput()
        {
            _horizontalInput = Input.GetAxis("Horizontal");
            _verticalInput = Input.GetAxis("Vertical");
        }
    }
}
