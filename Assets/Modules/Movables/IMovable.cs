﻿using Core.UniversalComponents;
using UnityEngine;

namespace Modules.Movables
{
    public interface IMovable : IUpdatable
    {
        public Vector3 MoveVector { get; }

        public void CalculateMove();
    }
}