using UnityEngine;

namespace Modules.UniversalMatch
{
    public interface IParticipant
    {
        public ParticipantTeam Team { get; set; }
    }
}
