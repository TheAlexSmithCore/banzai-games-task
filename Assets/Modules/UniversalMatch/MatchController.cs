using System;
using System.Collections.Generic;
using Core.UniversalComponents;
using UnityEngine;

namespace Modules.UniversalMatch
{
    public abstract class MatchController : UniversalComponentMono
    {
        public MatchDataStorage data;

        protected MatchState MatchState;

        private readonly List<IParticipant> _participants = new List<IParticipant>();

        public Action<MatchData, MatchState> OnMatchStateChanged = delegate { };

        private void Start()
        {
            Init();
        }

        protected abstract void Init();

        protected virtual void MatchStateChanged()
        {
            OnMatchStateChanged.Invoke(data.matchData, MatchState);
        }

        protected void ChangeMatchState(MatchState state)
        {
            MatchState = state;

            MatchStateChanged();
        }

        public void AddParticipant(IParticipant participant)
        {
            if (!_participants.Contains(participant))
            {
                _participants.Add(participant);
            }
        }

        public void RemoveParticipant(IParticipant participant)
        {
            if (_participants.Contains(participant))
            {
                _participants.Remove(participant);
            }
        }
    }
}
