namespace Modules.UniversalMatch
{
    public enum MatchState : byte
    {
        Prepare = 0,
        Start = 1,
        End = 2
    }
}