using Modules.Entities;
using Modules.UniversalMatch.UI;
using UnityEngine;

namespace Modules.UniversalMatch
{
    public class ArenaMatchController : MatchController
    {
        [SerializeField] private EntitySpawner spawner;
        [SerializeField] private MatchUIController uiController;

        private float _convertedToVisualizeTimer;
        private float _lastResetTimerValue;
        private float _matchTimer;

        protected override void Init()
        {
            SpawnEntities();

            OnMatchStateChanged += spawner.OnMatchStateChanged;
            spawner.OnLocalPlayerDied += LocalPlayerDied;
        }

        protected override void MatchStateChanged()
        {
            ResetTimer();
            
            switch (MatchState)
            {
                case MatchState.Prepare:
                    MatchPrepare();
                    break;
                case MatchState.Start:
                    MatchStart();    
                    break;
                case MatchState.End:
                    MatchEnd();
                    break;
            }
            
            uiController.OnMatchStateChanged(MatchState);
            
            base.MatchStateChanged();
        }
        
        private void ResetTimer()
        {
            _lastResetTimerValue = Time.time;
        }
        
        private void CalculateTimer()
        {
            _matchTimer = Time.time - _lastResetTimerValue;

            if (MatchState == MatchState.Prepare)
            {
                _convertedToVisualizeTimer = data.matchData.prepareTime - _matchTimer + 1;
                
                if (_matchTimer > data.matchData.prepareTime)
                {
                    ChangeMatchState(MatchState.Start);
                } 
            }

            if (MatchState == MatchState.Start)
            {
                _convertedToVisualizeTimer = data.matchData.matchDuration - _matchTimer + 1;
                
                if (_matchTimer > data.matchData.matchDuration)
                {
                    ChangeMatchState(MatchState.End);
                }
            }
      
            uiController.RefreshTimer(_convertedToVisualizeTimer);
        }

        private void LocalPlayerDied(ISpawnable localPlayer)
        {
            ChangeMatchState(MatchState.End);
        }
        
        private void Update()
        {
            if(!IsActive) {return;}
            
            CalculateTimer();
        }

        private void MatchPrepare()
        {
            // Тут что-то
        }

        private void MatchStart()
        {
            // И тут что-то
        }

        private void MatchEnd()
        {
            // Ну а тут всё
        }

        private async void SpawnEntities()
        {
            await spawner.Spawn(data.matchData);
            ChangeMatchState(MatchState.Prepare);
        }
    }
}
