using System;
using UnityEngine;
using UnityEngine.UI;

namespace Modules.UniversalMatch.UI
{
    public class MatchTimerView : MonoBehaviour
    {
        [SerializeField] private Text timerText;
        [SerializeField] [Multiline] private string timerFormat;

        private TimeSpan _convertedTime;
        
        public void Refresh(float value)
        {
            _convertedTime = TimeSpan.FromSeconds(value);
            
            timerText.text = _convertedTime.ToString(timerFormat);;
        }
    }
}
