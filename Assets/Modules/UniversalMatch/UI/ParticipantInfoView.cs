using Core.Extendable;
using UnityEngine;
using UnityEngine.UI;

namespace Modules.UniversalMatch.UI
{
    public class ParticipantInfoView : MonoBehaviour
    {
        [SerializeField] private Image healthFill;

        public void Refresh(INormalizable healthNormalizable)
        {
            healthFill.fillAmount = healthNormalizable.Normalized();
        }
    }
}
