using Core.UniversalComponents;
using UnityEngine;

namespace Modules.UniversalMatch.UI
{
    public class MatchUIController : UniversalComponentMono
    {
        [SerializeField] private StateChangeAction[] stateChangeActions;

        [Header("UI Elements")] 
        [SerializeField] private MatchTimerView[] timers;
        
        public void OnMatchStateChanged(MatchState state)
        {
            foreach (StateChangeAction changeAction in stateChangeActions)
            {
                if (changeAction.matchState == state)
                {
                    changeAction.Call();
                    continue;
                }
                
                changeAction.InverseCall();
            }
        }

        public void RefreshTimer(float value)
        {
            foreach (MatchTimerView timer in timers)
            {
                if(!timer.isActiveAndEnabled){continue;}
                
                timer.Refresh(value);
            }
        }
    }
}
