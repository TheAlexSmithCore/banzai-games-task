using UnityEngine.Events;

namespace Modules.UniversalMatch.UI
{
    [System.Serializable]
    public class StateChangeAction
    {
        public MatchState matchState;
        public UnityEvent<bool> action;

        public void Call()
        {
            action.Invoke(true);
        }
        
        public void InverseCall()
        {
            action.Invoke(false);
        }
    }
}
