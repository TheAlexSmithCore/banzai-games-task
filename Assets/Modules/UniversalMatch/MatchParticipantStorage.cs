using UnityEngine;

namespace Modules.UniversalMatch
{
    [CreateAssetMenu(fileName = "NewMatchParticipantStorage", menuName = "UniversalMatch/MatchParticipantStorage", order = 51)]
    public class MatchParticipantStorage : ScriptableObject
    {
    }
}
