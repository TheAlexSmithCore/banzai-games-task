namespace Modules.UniversalMatch
{
    [System.Serializable]
    public class ParticipantData
    {
        public ParticipantData(bool isPlayer, int entityDataID)
        {
            this.isPlayer = isPlayer;
            this.entityDataID = entityDataID;
        }
        
        public bool isPlayer;

        public int entityDataID;
    }
}
