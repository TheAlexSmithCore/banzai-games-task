using UnityEngine;

namespace Modules.UniversalMatch
{
    public class SpawnPoint : MonoBehaviour, ISpawnPoint
    {
        [SerializeField] private float radius;

        private static readonly Color GizmosColor = Color.blue;

        public Vector3 CalculateRandomPointInRadius()
        {
            Vector3 randomPoint = transform.position + Random.onUnitSphere * radius;
            randomPoint.y = 0;

            return randomPoint;
        }
        
        private void OnDrawGizmos()
        {
            Gizmos.color = GizmosColor;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }   
}
