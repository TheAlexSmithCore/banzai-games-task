using UnityEngine;

namespace Modules.UniversalMatch
{
    [System.Serializable]
    public class MatchData
    {
        public float matchDuration;
        public float prepareTime;
        [Space]
        public MatchScenario[] matchScenarios;
    }
}
