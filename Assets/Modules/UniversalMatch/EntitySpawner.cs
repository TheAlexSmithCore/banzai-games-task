using System;
using System.Threading.Tasks;
using Core.UniversalComponents;
using Modules.CustomCamera;
using Modules.DataModels;
using Modules.Entities;
using Modules.Entities.Builders;
using Modules.Entities.Data;
using Modules.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Modules.UniversalMatch
{
    public class EntitySpawner : UniversalComponentMono
    {
        public ParticipantTeam localPlayerTeam;
        
        public CameraController сameraController;
        
        private ParticipantAggregator _participantAggregator;
        
        private EntityDataModel _entityDataModel;
        private WeaponDataModel _weaponDataModel;

        [HideInInspector]
        public SpawnPoint[] spawnPoints;
        
        public Action<ISpawnable> OnLocalPlayerDied = delegate {  }; 
        
        private void Awake()
        {
            _participantAggregator = new ParticipantAggregator();
            _entityDataModel = new EntityDataModel();
            _weaponDataModel = new WeaponDataModel();
        }

        public async Task Spawn(MatchData data)
        {
            foreach (MatchScenario scenario in data.matchScenarios)
            {
                await CreateEntity(scenario.Team, scenario.participantCount);
            }
        }

        private async Task CreateEntity(ParticipantTeam team, int count)
        {
            if (team == localPlayerTeam)
            {
                await SpawnLocalPlayer();
                count--;
            }
            
            for (int i = 0; i < count; i++)
            {
                await AssembleEntity(team, _entityDataModel.GetRandomEnemyID(), 1);
            }
        }

        private async Task SpawnLocalPlayer()
        {
            PlayerEntity playerEntity = (PlayerEntity)await AssembleEntity(ParticipantTeam.Red,0, 0);
            сameraController.ChangeTarget(playerEntity.Owner);
            сameraController.CameraPositionConverter.Add(playerEntity.weaponHolder);

            playerEntity.OnDead += LocalPlayerDied;
        }

        private async Task<Entity> AssembleEntity(ParticipantTeam team, int dataID, int controllerID)
        {
            EntityData entityData = _entityDataModel.Get(dataID);
                        
            ResourcesRequester<Entity> requester = new ResourcesRequester<Entity>();
            await requester.RequestAsync(_entityDataModel.GetControllerPath(controllerID));
            Entity entity = Instantiate(requester.Get());
            
            FirstSpawn(entity);
            
            ResourcesRequester<BodyModificator> bodyRequester = new ResourcesRequester<BodyModificator>();
            await bodyRequester.RequestAsync(entityData.BodyResourcePath);
            BodyModificator modificator = Instantiate(bodyRequester.Get(), entity.bodyTransform, false);

            EntityBuilder builder = new EntityBuilder(entity);
            builder.Build(team, entityData, modificator, _weaponDataModel, _participantAggregator);

            entity.OnDead += EntityDied;

            return entity;
        }

        private void LocalPlayerDied(ISpawnable localPlayer)
        {
            OnLocalPlayerDied.Invoke(localPlayer);
        }
        
        private void EntityDied(ISpawnable spawnable)
        {
            RespawnEntity(spawnable);
        }

        private void FirstSpawn(ISpawnable spawnable)
        {
            Vector3 spawnPoint = GetAvailableSpawnPoint();

            ObjectPuller puller = new ObjectPuller(spawnPoint, spawnable);
            puller.TranslateAll(spawnPoint);
            
            spawnable.Spawn();   
        }

        private void RespawnEntity(ISpawnable spawnable)
        {
            Vector3 spawnPoint = GetAvailableSpawnPoint();

            ObjectPuller puller = new ObjectPuller(spawnPoint, spawnable);
            puller.Pull();
            
            spawnable.Spawn();
        }

        private Vector3 GetAvailableSpawnPoint()
        {
            Vector3 randomPoint = Vector3.zero;

            while (сameraController.CameraPositionConverter.IsObjectInViewport(randomPoint))
            {
                randomPoint = GetRandomSpawnPoint();
            }

            return randomPoint;
        }

        private Vector3 GetRandomSpawnPoint()
        {
            return spawnPoints[Random.Range(0, spawnPoints.Length - 1)].CalculateRandomPointInRadius();
        }

        public void OnMatchStateChanged(MatchData data, MatchState state)
        {
            if (state == MatchState.End || state == MatchState.Prepare)
            {
                _participantAggregator.SetAllEntityActivationState(false);
            }
            else
            {
                _participantAggregator.SetAllEntityActivationState(true);
            }
        }
    }
}
