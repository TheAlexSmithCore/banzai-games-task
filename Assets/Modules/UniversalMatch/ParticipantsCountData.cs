namespace Modules.UniversalMatch
{
    [System.Serializable]
    public class ParticipantsCountData
    {
        public ParticipantTeam Team;
        public int MaxCount;
    }
}
