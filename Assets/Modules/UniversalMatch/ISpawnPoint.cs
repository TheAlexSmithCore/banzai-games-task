using UnityEngine;

namespace Modules.UniversalMatch
{
    public interface ISpawnPoint
    {
        public Vector3 CalculateRandomPointInRadius();
    }
}
