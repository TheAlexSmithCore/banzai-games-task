using System.Collections.Generic;
using Modules.Entities;
using UnityEngine;

namespace Modules.UniversalMatch
{
    public enum ParticipantTeam : byte
    {
        Red = 0,
        Blue = 1
    }
    
    public class ParticipantAggregator
    {
        private readonly Dictionary<ParticipantTeam, List<Entity>> _participantsData = new Dictionary<ParticipantTeam, List<Entity>>(); 

        public IDamageable[] GetEnemies(ParticipantTeam alliesTeam)
        {
            List<IDamageable> result = new List<IDamageable>();
            
            foreach (KeyValuePair<ParticipantTeam, List<Entity>> participants in _participantsData)
            {
                if(participants.Key == alliesTeam) { continue; }
                
                result.AddRange(participants.Value);
            }

            return result.ToArray();
        }
        
        public void SetAllEntityActivationState(bool state)
        {
            foreach (var entity in _participantsData.Values)
            {
                for (int i = 0; i < entity.Count; i++)
                {
                    entity[i].IsActive = state;
                }
            }
        }

        public void Add(ParticipantTeam team, Entity target)
        {
            if (_participantsData.ContainsKey(team))
            {
                _participantsData[team].Add(target);
            }
            else
            {
                _participantsData.Add(team, new List<Entity> {target});
            }
        }
    }
}
