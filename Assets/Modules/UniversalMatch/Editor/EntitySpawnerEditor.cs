using UnityEditor;
using UnityEngine;

namespace Modules.UniversalMatch.EditorScripts
{
    [CustomEditor(typeof(EntitySpawner))]
    public class EntitySpawnerEditor : Editor
    {
        private EntitySpawner _spawner;

        private void OnEnable()
        {
            _spawner = (EntitySpawner) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            
            if (GUILayout.Button("Recalculate Spawn Points", GUILayout.Height(30f)))
            {
                RecalculateSpawnPoints();
            }
            GUILayout.Label("Spawn Points Cashed: " + _spawner.spawnPoints.Length, EditorStyles.miniLabel);
        }

        private void RecalculateSpawnPoints()
        {
            SpawnPoint[] points = (SpawnPoint[])FindObjectsOfType(typeof(SpawnPoint));

            _spawner.spawnPoints = points;
        }
    }
}
