using UnityEngine;

namespace Modules.UniversalMatch
{
    [CreateAssetMenu(fileName = "New Match Data", menuName = "UniversalMatch/MatchData", order = 51)]
    public class MatchDataStorage : ScriptableObject
    {
        public MatchData matchData;
    }
}

